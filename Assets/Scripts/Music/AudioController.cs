﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour
{
    public float VolumeDelay = 1;
    public float HighVolume = 1;
    public float LowVolume = 0.2f;

    void Awake()
    {
        audio.playOnAwake = false;
        audio.clip = FindObjectOfType<ArrowController>().Generator.Music;
    }

    void Start()
    {

        ArrowController.Cur.FinishedHandlers += TurnDownTheVolume;
        FindObjectOfType<NinjaJumper>().StopHandlers += TurnUpTheVolume;
    }

    public void Play()
    {
        audio.Play();
        Debug.Log(audio.clip);
    }

    public void TurnDownTheVolume(List<bool> results)
    {
        Debug.Log("down!!!!");
        StartCoroutine(VolumeEdit(audio.volume, LowVolume, 0));
    }

    public void TurnUpTheVolume()
    {
        StartCoroutine(VolumeEdit(audio.volume, HighVolume, ArrowController.Cur.FallTime - VolumeDelay));
    }

    public IEnumerator VolumeEdit(float startVolume, float endVolume, float delay)
    {
        yield return new WaitForSeconds(delay);
        float start = Time.time;
        while (Time.time - start < VolumeDelay)
        {
            audio.volume = Mathf.Lerp(startVolume, endVolume, (Time.time - start) / VolumeDelay);
            yield return null;
        }
        
        audio.volume = endVolume;
        yield return null;
    }
	
}
