﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class MusicalArrowGenerator : MonoBehaviour
{
    public AudioClip Music;
    public float Bpm = 1;
    public float Delta = 0;

    public List<float> Times;
    public List<ArrowController.Direction> Directions; 

    public void Generate()
    {
        Times = new List<float>();
        Directions = new List<ArrowController.Direction>();

        if (Bpm < 1)
        {
            throw new ArgumentException();
        }
        for (int i = 0; i < Music.length * Bpm / 60; i++)
        {
            Times.Add(i * 60 / Bpm + Delta);
            Directions.Add((ArrowController.Direction)Random.Range(0, 3));
        }
    }
}
