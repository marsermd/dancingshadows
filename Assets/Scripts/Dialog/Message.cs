﻿using System;
using UnityEngine;
using System.Collections;

public class Message : MonoBehaviour
{
    public delegate void Finished();

    public Finished FinishedHandlers = delegate { };

    public float LifeTime;
    public String Content;
    public GUISkin Skin;
    public Rect WindowRect;

    private float _startTime;

    void Start()
    {
        _startTime = Time.time;
    }

    void Update()
    {

        if (Time.time > _startTime + LifeTime)
        {
            FinishedHandlers();
            Destroy(gameObject);
        }
    }

    void OnGUI()
    {
        GUI.skin = Skin;

        GUI.Window(0, WindowRect, WindowFunction, Content);
    }

    void WindowFunction(int windowID)
    {
    }
}
