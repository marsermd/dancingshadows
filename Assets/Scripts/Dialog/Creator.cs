﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Creator : MonoBehaviour
{
    public delegate void Finished();
    public Finished FinishedHandlers = delegate { };

    public GUISkin Skin; 
    public Rect WindowRect = new Rect(0, 50, 700, 120);
    
    public List<Message> Messages;
    public AudioClip Intro;
    private Message message;
    private int id = 0;
    void Start()
    {
        gameObject.AddComponent<AudioSource>();
        audio.clip = Intro;
        audio.Play();
        FinishedHandlers += Stop;

        WindowRect.x = Screen.width/2 - WindowRect.width/2;
        WindowRect.y = Screen.height - WindowRect.height - WindowRect.y;
        Play();
    }

    void Play()
    {
        if (id >= Messages.Count)
        {
            FinishedHandlers();
            return;
        }
        message = Instantiate(Messages[id]) as Message;
        message.FinishedHandlers += Play;
        message.Skin = message.Skin ?? Skin;
        if (message.WindowRect.width == 0)
        {
            message.WindowRect = WindowRect;
        }
        id++;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            Destroy(message.gameObject);
            FinishedHandlers();
            Stop();
        }
    }

    void Stop()
    {
        audio.Stop();
    }

    void WindowFunction(int windowID)
    {
        // Draw any Controls inside the window here
    }
}
