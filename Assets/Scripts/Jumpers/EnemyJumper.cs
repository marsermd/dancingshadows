﻿using UnityEngine;
using System.Collections;

public class EnemyJumper : MonoBehaviour{

    public WayPath SelectedWay;
    private Enemy SelectedEnemy;
    public float TimePerJump = 0.2f;
    public float StartTime = 0.5f;
    private bool Stopped = true;

    void Start()
    {
        SelectedEnemy = GetComponent<Enemy>();
    }

    void Update()
    {
        if (Stopped)
        {
            StartCoroutine(DoJump());
        }
    }

    private IEnumerator DoJump()
    {
        Stopped = false;

        float lastTime = Time.time;

        for (int i = 0; i < SelectedWay.Points.Length - 1; i++)
        {
            var point = SelectedWay.Points[i];
            Debug.Log(i + " " + (lastTime + point.JumpTime - Time.time));
            point.Apply(SelectedEnemy);
            Debug.Log(i + " " + (lastTime + point.JumpTime - Time.time));
            SelectedEnemy.Jump(point, point.JumpTime);
            Debug.Log(i + " " + (lastTime + point.JumpTime - Time.time));

            while (lastTime + point.JumpTime > Time.time)
            {
                yield return null;
            }
            lastTime = Time.time;
        }
        Debug.Log("sd");
        for (int i = SelectedWay.Points.Length - 1; i > 0 ; i--)
        {
            var point = SelectedWay.Points[i];
            //Debug.Log(i + " " + (lastTime + point.JumpTime - Time.time));
            point.Apply(SelectedEnemy);
            SelectedEnemy.Jump(point, point.JumpTime);

            while (lastTime + point.JumpTime > Time.time)
            {
                yield return null;
            }
            lastTime = Time.time;
        }
        Stopped = true;
        yield return null;
    }
}
