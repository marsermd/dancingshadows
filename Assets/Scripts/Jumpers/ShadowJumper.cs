﻿using System.Reflection;
using UnityEngine;
using System.Collections;

public class ShadowJumper : MonoBehaviour
{
    public Way SelectedWay;
    public Ninja Ninja;
    public float JumpTime = 0.2f;

	void Start ()
	{
	    ArrowController.Cur.ArrowHandlers += Jump;
	}

    private int _nextId = 0;

    void Jump(bool ok)
    {
        BasePoint point;
        if (ok)
        {
            point = SelectedWay.Points[_nextId];
        }
        else
        {
            point = SelectedWay.Points[_nextId].Fail;
        }

        point.Apply(Ninja);
        float animTime = Ninja.Animation.state.GetCurrent(0).Animation.duration;
        float scale = animTime / JumpTime;
        Ninja.Animation.timeScale = scale;
        Ninja.Jump(point, JumpTime);
        
        _nextId++;
    }
}
