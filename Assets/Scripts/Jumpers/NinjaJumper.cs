﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;

public class NinjaJumper : MonoBehaviour
{
    public delegate void JumpStop();

    public JumpStop StopHandlers = delegate { };

    public Way SelectedWay;
    public Ninja SelectedNinja;
    public float TimePerJump = 0.2f;
    public float StartTime = 0.5f;
    private int _curPath = 0;
    
    void Start()
    {
        ArrowController.Cur.FinishedHandlers += Launch;
    }

	public void Launch (List<bool> results) {
	    StartCoroutine(DoJump(results));
	}

    private IEnumerator DoJump(List<bool> results)
    {
        float start = Time.time;
        while (Time.time < start + FindObjectOfType<AudioController>().VolumeDelay)
        {
            yield return null;
        }

        float lastTime = Time.time;

        for (int i = 0; i < results.Count; i++)
        {
            BasePoint point;
            if (results[i])
            {
                point = SelectedWay.Pathes[_curPath].Points[i];
                SelectedWay.Pathes[_curPath].Points[i].Evaluate(true);
            }
            else
            {
                point = SelectedWay.Pathes[_curPath].Points[i].Fail;
            }
            point.Apply(SelectedNinja);
            point.PlayEffects(SelectedNinja);
            SelectedNinja.Jump(point, point.JumpTime);

            while (lastTime + point.JumpTime > Time.time)
            {
                yield return null;
            }
            lastTime = Time.time;
	    }

        _curPath++;
        StopHandlers();
        yield return null;
    }
}
