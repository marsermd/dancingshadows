﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour
{
    public ArrowController.Direction Dir;
    public float EndTime;
    public int Id;
    public bool Activated = false;
}
