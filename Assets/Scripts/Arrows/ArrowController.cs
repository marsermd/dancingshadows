﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using UnityEngine;
using System.Collections;

public class ArrowController : MonoBehaviour
{
    private const float EPS_TIME = 2;
    public enum Direction
    {
        Left = 0, Up = 1, Right = 2, None = 999
    }

    public delegate void ArrowEvent(bool right);

    public delegate void FinishedEvent(List<bool> userResults);

    public ArrowEvent ArrowHandlers = delegate { };
    public FinishedEvent FinishedHandlers = delegate { };

    public static ArrowController Cur;
    public MusicalArrowGenerator Generator;

    public Arrow Left;
    public Arrow Up;
    public Arrow Right;

    public float FallTime = 4;
    public float Height = 5;
    public float Padding = 0.2f;
    public float DeltaTime = 0.4f;

    public float SoundUpTime = 5;

    public Way SelectedWay;
    public NinjaJumper Jumper;

    public List<Direction> Directions;
    public List<float> Times;

    public String NextLevel;

    private bool _paused = false;

    private Queue<Arrow> Falling; 
    
    private List<bool> UserResults;

    private SacuraBehaviour Sacura;

    private int _nextId;
    private int _startId;
    private int _pathId;

    private AudioController _audioController;

    public GUISkin Skin;

    void Awake()
    {
        Cur = this;
        _audioController = FindObjectOfType<AudioController>();
    }

    void Start()
    {
        Sacura = FindObjectOfType<SacuraBehaviour>();
        Jumper = FindObjectOfType<NinjaJumper>();
        Jumper.StopHandlers += Unpause;
        Generator.Generate();
        Directions = Generator.Directions;
        Times = Generator.Times;

        Init();
        FindObjectOfType<Creator>().FinishedHandlers += Launch;

    }

    private IEnumerator SelfLaunch()
    {
        yield return new WaitForSeconds(2);
        Launch();
    }

    public void Init()
    {
        Falling = new Queue<Arrow>();
        _nextId = 0;
        _pathId = 0;
        _startId = 0;
    }

    public void Launch()
    {
        _audioController.Play();

        StartCoroutine(LaunchCoroutine());
    }

    public void Unpause()
    {
        _paused = false;
    }
    
    public IEnumerator LaunchCoroutine()
    {
        while (_pathId < SelectedWay.Pathes.Length)
        {
            while (Times[_startId] < _audioController.audio.time + FallTime)
            {
                _startId++;
            }

            _nextId = _startId;

            UserResults = new List<bool>();
            while (_startId == _nextId || Falling.Count != 0)
            {
                EmitArrows();
                MoveArrows();
                HandleInput();
                RemoveArrows();

                yield return null;
            }

            _paused = true;

            FinishedHandlers(UserResults);

            while (_paused)
            {
                yield return null;
            }

            if (UserResults.Count(value => !value) > 2)
            {
                CreateMessage("I have failed the mission.. I'd better wake up right now!").FinishedHandlers += Restart;
                yield return new WaitForSeconds(4);
            }
            _pathId++;
        }
        CreateMessage("Success. That's the only thing i accept!").FinishedHandlers += ContinueLevels;
        yield return null;
    }

    public void ContinueLevels()
    {
        Application.LoadLevel(NextLevel);
    }

    public void Restart()
    {
        Application.LoadLevel(Application.loadedLevelName);
    }

    private void EmitArrows()
    {
        if (_nextId < _startId + SelectedWay.Pathes[_pathId].Points.Length && Times[_nextId] < _audioController.audio.time + FallTime)
        {
            var cur = Instantiate(GetArrow(Directions[_nextId])) as Arrow;
            cur.EndTime = Times[_nextId];
            cur.Id = _nextId;
            cur.Dir = Directions[_nextId];
            Falling.Enqueue(cur);
            _nextId++;
        }
    }

    private void MoveArrows()
    {
        foreach (Arrow arrow in Falling)
        {
            Vector3 x = ((int)arrow.Dir) * Vector3.right * (Left.renderer.bounds.size.x + Padding) + Left.renderer.bounds.size.x * 0.5f * Vector3.right;
            Vector3 y = Vector3.down * Left.renderer.bounds.size.y * 0.5f;
            arrow.transform.position = transform.position + Vector3.up * (arrow.EndTime - (_audioController.audio.time)) * Height / FallTime + x + y;
        }
    }

    private void HandleInput()
    {
        if (GetPressedDirection() != Direction.None && Falling.Count != 0)
        {
            if (Falling.Peek().Activated)
            {
                return;
            }

            bool inTime = Math.Abs(_audioController.audio.time - Falling.Peek().EndTime) < DeltaTime;
            bool rightDir = Falling.Peek().Dir == GetPressedDirection();
            UserResults.Add(rightDir && inTime);
            Falling.Peek().Activated = true;
            if (rightDir && inTime)
            {
                Falling.Peek().renderer.material.color = Color.green;
            }
            else
            {
                Falling.Peek().renderer.material.color = Color.red;
            }
            ArrowHandlers(rightDir && inTime);
        }
    }

    private void RemoveArrows()
    {
        while (Falling.Count != 0 && _audioController.audio.time - DeltaTime > Falling.Peek().EndTime)
        {
            if (!Falling.Peek().Activated)
            {
                UserResults.Add(false);
                ArrowHandlers(false);
            }
            Destroy(Falling.Dequeue().gameObject);
        }
    }

    private Arrow GetArrow(Direction dir)
    {
        switch (dir)
        {
            case Direction.Left:
                return Left;
            case Direction.Up:
                return Up;
            case Direction.Right:
                return Right;
            default:
                throw new ArgumentException("unhanded direction " + dir);
        }
    }

    private Direction GetPressedDirection()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow)) 
            return Direction.Left;
        if (Input.GetKeyDown(KeyCode.UpArrow)) 
            return Direction.Up;
        if (Input.GetKeyDown(KeyCode.RightArrow)) 
            return Direction.Right;
        return Direction.None;
    }

    private Message CreateMessage(String val)
    {
        GameObject holder = new GameObject("transition");
        Message message = holder.AddComponent<Message>();
        message.Skin = Skin;
        message.LifeTime = 3;
        message.WindowRect = GetRect();
        message.Content = val;
        return message;
    }

    private Rect GetRect()
    {
        Rect tmp = new Rect(0, 50, 700, 120);
        tmp.x = Screen.width / 2 - tmp.width / 2;
        tmp.y = Screen.height - tmp.height - tmp.y;
        return tmp;
    }

//
//    private void _lastUpdate();
//
//    private float getTime()
//    {
//        return 
//    }
}
