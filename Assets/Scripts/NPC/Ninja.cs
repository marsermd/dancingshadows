﻿using UnityEngine;
using System.Collections;

public class Ninja : Dieable
{
    public AnimationCurve SpeedOverJump;

    public SkeletonAnimation Animation;

    void Start()
    {
        Animation = GetComponent<SkeletonAnimation>();
    } 

    public virtual void Jump(BasePoint point, float time)
    {
        StopCoroutine("ActionCoroutione");
        StartCoroutine(ActionCoroutine(point, time));
    }

    public virtual IEnumerator ActionCoroutine(BasePoint point, float time)
    {
        Vector3 start = transform.position;
        Vector3 end = point.transform.position;
        float startTime = Time.time;
        while (Time.time < startTime + time)
        {
            transform.position = Vector3.Lerp(start, end, point.NinjaPosition.Evaluate(getScaledTime(Time.time - startTime, time, point)));
            yield return new WaitForEndOfFrame();
        }
        transform.position = end;

        point.Deply(this);
        yield return null;
    }

    public float getScaledTime(float curTime, float maxTime, BasePoint point)
    {
        float scaledTime = curTime / maxTime;
        return scaledTime * point.JumpTime;
    }
	
}
