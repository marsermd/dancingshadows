﻿using System;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using System.Collections;

public class Enemy : Ninja
{

    public String DieAnimation = "death";

    void Start()
    {
        Animation = GetComponent<SkeletonAnimation>();
    }

    public override void Jump(BasePoint point, float time)
    {
        StopCoroutine("ActionCoroutione");
        StartCoroutine(ActionCoroutine(point, time));
        if (Marked)
        {
            Destroy(GetComponent<EnemyJumper>());
        }
    }

    public override IEnumerator ActionCoroutine(BasePoint point, float time)
    {
        Vector3 start = transform.position;
        Vector3 end = point.transform.position;
        float startTime = Time.time;
        while (Time.time < startTime + time && !Marked)
        {
            transform.position = Vector3.Lerp(start, end, point.NinjaPosition.Evaluate(Time.time - startTime));
            yield return new WaitForEndOfFrame();
        }
        if (!Marked)
        {
            transform.position = end;
        }
        else
        {
            Animation.state.SetAnimation(0, "stand", false);
        }

        point.Deply(this);
        yield return null;
    }

    public override void Die()
    {
        Animation.state.SetAnimation(0, DieAnimation, false);
        Animation.timeScale = 2;
        FindObjectOfType<NinjaJumper>().StopHandlers -= Die;
    }
}
