﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Dieable : MonoBehaviour
{
    protected bool Marked = false;

    public void GetHit(bool success)
    {
        if (success)
        {
            Debug.Log("subscribed!");
            FindObjectOfType<NinjaJumper>().StopHandlers += Die;
            Marked = true;
        }
    }

    public virtual void Die()
    {
        Debug.Log("dead");
        FindObjectOfType<NinjaJumper>().StopHandlers -= Die;
        Destroy(gameObject);
    }
}
