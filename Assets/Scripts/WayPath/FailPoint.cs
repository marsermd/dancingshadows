﻿using UnityEngine;
using System.Collections;

public class FailPoint : BasePoint {

    public override void Apply(Ninja ninja)
    {
        ninja.Animation.loop = false;
        base.Apply(ninja);
    }


    public override void Deply(Ninja ninja)
    {
        ReachedHandlers();
        ninja.Animation.state.Update(0);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 0.6f);
    }
}
