﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class WayPath : MonoBehaviour
{
    private WayPoint[] _points = null;

    public bool DrawGoodWays = true;
    public bool DrawBadWays = true;

    public WayPoint[] Points
    {
        get
        {
            if (_points == null || Application.platform == RuntimePlatform.WindowsEditor)
            {
                _points = GetComponentsInChildren<WayPoint>();
            }
            return _points;
        }
        private set { _points = value; }
    }

	void Start () {
        Points = GetComponentsInChildren<WayPoint>();
	}

    void OnDrawGizmos ()
    {
        for (int i = 0; i < Points.Length; i++)
        {
            if (DrawGoodWays)
            {
                Gizmos.color = Color.Lerp(Color.green, Color.yellow, (float) i/Points.Length);
                if (i < Points.Length - 1)
                {
                    Gizmos.DrawLine(Points[i].transform.position, Points[i + 1].transform.position);
                }
                Gizmos.DrawWireSphere(Points[i].transform.position, 0.3f);
            }
        }
    }
}
