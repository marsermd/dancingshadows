﻿using System.Linq;
using UnityEngine;
using System.Collections;

public class BasePoint : MonoBehaviour {
    public AnimationCurve NinjaPosition = AnimationCurve.EaseInOut(0, 0, 1, 1);
    public string AnimationName;
    public float AnimationSpeed = 1;
    public bool Stop;

    public BaseEffect[] Effects;

    public delegate void Reached();

    public Reached ReachedHandlers = delegate { };


    public float JumpTime
    {
        get { return NinjaPosition.keys.Last().time; }
    }

    public void PlayEffects(Ninja ninja)
    {
        foreach (BaseEffect effectPref in Effects)
        {
            BaseEffect effect = Ninja.Instantiate(effectPref) as BaseEffect;
            effect.CurPoint = this;
            effect.PrevPos = ninja.transform.position;
            effect.SelectedNinja = ninja;
            effect.transform.parent = ninja.transform;
            effect.transform.localPosition = Vector3.zero;
            effect.Launch();
        }
    }

    public virtual void Apply(Ninja ninja)
    {
        ninja.Animation.state.SetAnimation(0, AnimationName, false);

        ninja.Animation.timeScale = AnimationSpeed;
        ninja.Animation.skeleton.FlipX = ninja.transform.position.x < transform.position.x;
    }

    public virtual void Deply(Ninja ninja)
    {
        
    }
}
