﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;

public class Way : MonoBehaviour {

    private WayPath[] _pathes = null;

    public WayPath[] Pathes
    {
        get
        {
            if (_pathes == null || Application.platform == RuntimePlatform.WindowsEditor)
            {
                _pathes = GetComponentsInChildren<WayPath>();
            } 
            return _pathes;
        }
        private set { _pathes = value; }
    }

    public List<WayPoint> Points
    {
        get
        {
            List<WayPoint> points = new List<WayPoint>();
            foreach (WayPath path in Pathes)
            {
                points.AddRange(path.Points);
            }
            return points;
        }
    }

    void OnDrawGizmos ()
    {
        for (int i = 0; i < Pathes.Length; i++)
        {
            Gizmos.color = Color.red;
            if (i < Pathes.Length - 1)
            {
                Gizmos.DrawLine(Pathes[i].Points.Last().transform.position, Pathes[i + 1].Points.First().transform.position);
            }
        }
    }

    void Start()
    {
        Pathes = GetComponentsInChildren<WayPath>();
    }
}
