﻿using System.Linq;
using UnityEngine;
using System.Collections;

public class WayPoint : BasePoint
{
    public Dieable[] Victims;
    public bool Loop;

    public FailPoint Fail
    {
        get { return GetComponentInChildren<FailPoint>(); }
    }

    public void Evaluate(bool success)
    {
        
        foreach (Dieable victim in Victims)
        {
            victim.GetHit(success);
        }
    }

    public override void Apply(Ninja ninja)
    {
        ninja.Animation.loop = Loop;

        base.Apply(ninja);
    }

    public override void Deply(Ninja ninja)
    {
        if (Stop)
        {
            ninja.Animation.AnimationName = "stand";
        }
        ReachedHandlers();
    }

	// Update is called once per frame
    void OnDrawGizmos ()
    {
        Gizmos.DrawWireSphere(transform.position, 0.1f);
    }

}
