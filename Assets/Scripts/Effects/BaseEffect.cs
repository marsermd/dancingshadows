﻿using UnityEngine;
using System.Collections;

public class BaseEffect : MonoBehaviour
{
    public float BeginTime;
    public float LifeTime = 10;

    public Ninja SelectedNinja;
    public BasePoint CurPoint;
    public Vector3 PrevPos;

    private float _start;

    public void Launch()
    {
        _start = Time.time;
        StartCoroutine(WaitForIt());
        CurPoint.ReachedHandlers += Reached;
    }

    public IEnumerator WaitForIt()
    {
        while (Time.time < BeginTime + _start)
        {
            yield return new WaitForSeconds(BeginTime + _start - Time.time);
        }
        Execute();
        while (Time.time < BeginTime + _start + LifeTime)
        {
            yield return new WaitForSeconds(BeginTime + _start + LifeTime - Time.time);
        }
        Finish();
        if (gameObject != null)
        {
            CurPoint.ReachedHandlers -= Reached;
            Destroy(gameObject);
        }
    }

    public virtual void Execute()
    {
        
    }

    public virtual void Reached()
    {

    }

    public virtual void Finish()
    {

    }
}
