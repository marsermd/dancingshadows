﻿using UnityEngine;
using System.Collections;

public class Slash : MonoBehaviour {

    public float slashDuration = 0.2f;

    private bool visible = false;
    private Color visibleSlash;
    private Color invisibleSlash;

    private float startTime;

	// Use this for initialization
	void Start () {
        startTime = Time.time;
        visibleSlash = gameObject.renderer.material.color;
        invisibleSlash = new Color(visibleSlash.r, visibleSlash.g, visibleSlash.b, 0);
        gameObject.renderer.material.color = new Color(visibleSlash.r, visibleSlash.g, visibleSlash.b, 0);
	}
	
	// Update is called once per frame
	void Update () {
        float iterator = (Time.time - startTime) * slashDuration;
        if (!visible)
        {
            gameObject.renderer.material.color = Color.Lerp(gameObject.renderer.material.color, visibleSlash, iterator);
            visible = gameObject.renderer.material.color == visibleSlash;
        }
        else
        {
            gameObject.renderer.material.color = Color.Lerp(gameObject.renderer.material.color, invisibleSlash, iterator);
        }
	}
}
