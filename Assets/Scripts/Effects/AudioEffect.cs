﻿using UnityEngine;
using System.Collections;

public class AudioEffect : BaseEffect
{
    public AudioClip Sound;
    public bool Loop;
    public bool StopOnReach = true;

    public override void Execute()
    {
        gameObject.AddComponent<AudioSource>();
        audio.clip = Sound;
        audio.loop = Loop;
        audio.Play();
    }

    public override void Reached()
    {
        if (StopOnReach)
        {
            audio.Stop();
        }
    }

    public override void Finish()
    {
        audio.Stop();
    }
}
