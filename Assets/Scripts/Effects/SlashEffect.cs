﻿using UnityEngine;
using System.Collections;

public class SlashEffect : MonoBehaviour {

    public float slashStateZ = 0;
    public float startPointerX = -3;
    public float startPointerY = -2;
    public float endPointerX = 4;
    public float endPointerY = 5;
    public float slashDuration = 0.2f;
    public float timeToDestroy = 10f;
    public bool runTest = true;
    public int testsCount = 10;
    public GameObject pattern;

    private GameObject slash;

    void CreateSlash(Vector3 startPointer, Vector3 endPointer){
        slash = (GameObject) Instantiate(pattern);
        Vector3 scale = endPointer - startPointer;
        slash.transform.position = (endPointer + startPointer) / 2;
        slash.transform.localScale = new Vector3(scale.magnitude, 0.1f, 1);
        slash.transform.Rotate(0, 0, Mathf.Atan2(scale.y, scale.x) * Mathf.Rad2Deg);
        Destroy(slash, timeToDestroy);

    }

	// Use this for initialization
	void Start () {
        if (runTest)
        {
            StartCoroutine(TestCoroutine());
        }
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    IEnumerator TestCoroutine()
    {
        CreateSlash(new Vector3(startPointerX, startPointerY, slashStateZ), new Vector3(endPointerX, endPointerY, slashStateZ));
        for (int i = 0; i < testsCount; ++i)
        {
            yield return new WaitForSeconds(1f);
            CreateSlash(new Vector3(Random.Range(-10, 10), Random.Range(-4, 4), slashStateZ), new Vector3(Random.Range(-10, 10), Random.Range(-4, 4), slashStateZ));
        }
    }
}
