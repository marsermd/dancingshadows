﻿using UnityEngine;
using System.Collections;

public class SacuraBehaviour : MonoBehaviour
{

    public float minY = -5;
    public float maxY = 5;
    public float minX = -11;
    public float maxX = 11;
    public float sacuraLeafsStateZ = 0;
    public float minZ = -7f;
    public float maxZ = 11f;
    public static int particlesCount = 3500;

    private float[] startTime = new float[particlesCount];
    private float[] journeyLength = new float[particlesCount];
    private float[] speed = new float[particlesCount];
    private float[] direction = new float[particlesCount];
    private float[] multiplicator = new float[particlesCount];

    private Vector3[] startPosition = new Vector3[particlesCount];
    private Vector3[] endPosition = new Vector3[particlesCount];

    ParticleSystem.Particle[] leaf;
    Vector3[] nextWaypoint = {        
        new Vector3(0, -0.5F, 0),
        new Vector3(-0.3F, -0.4F, 0),
        new Vector3(-0.4F, 0, 0),
        new Vector3(0, -0.2F, 0),
        new Vector3(0.43F, -0.1F, 0),
        new Vector3(0, 0.1F, 0),};
    int[] states = new int[particlesCount];

    void Start()
    {
        particleSystem.Emit(particlesCount);
        leaf = new ParticleSystem.Particle[particleSystem.particleCount];
        particleSystem.GetParticles(leaf);
        for (int i = 0; i < leaf.Length; ++i)
        {
            speed[i] = Random.Range(0.8F, 1.3F);
            direction[i] = Mathf.Pow(-1, Random.Range(0, 2));
            multiplicator[i] = Random.Range(0.5f, 1f);
            leaf[i].size = multiplicator[i] * leaf[i].size;
            startTime[i] = Time.time;
            states[i] = Random.Range(0, nextWaypoint.Length);
            leaf[i].position = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), Random.Range(minZ, maxZ));
            startPosition[i] = nextWaypoint[states[i]];
            endPosition[i] = nextWaypoint[(states[i] + 1) % nextWaypoint.Length];
            journeyLength[i] = Vector3.Distance(startPosition[i], endPosition[i]);
        }
        particleSystem.SetParticles(leaf, particlesCount);
    }

    void Update()
    {
        leaf = new ParticleSystem.Particle[particleSystem.particleCount];
        particleSystem.GetParticles(leaf);
        for (int i = 0; i < leaf.Length; ++i)
        {
            if (leaf[i].position.y < minY)
            {
                leaf[i].position = new Vector3(leaf[i].position.x, (maxY - Mathf.Abs(leaf[i].position.y - minY)), leaf[i].position.z);
            }
            if (leaf[i].position.x > maxX)
            {
                leaf[i].position = new Vector3(minX, leaf[i].position.y, leaf[i].position.z);
            }
            if (leaf[i].position.y > maxY)
            {
                leaf[i].position = new Vector3(leaf[i].position.x, (minY + Mathf.Abs(leaf[i].position.y - maxY)), leaf[i].position.z);
            }
            if (leaf[i].position.x < minX)
            {
                leaf[i].position = new Vector3(maxX, leaf[i].position.y, leaf[i].position.z);
            }
            float distCovered = (Time.time - startTime[i]) * speed[i];
            float fracJourney = distCovered / journeyLength[i];
            leaf[i].position += Vector3.Lerp(startPosition[i], endPosition[i], fracJourney) / 20;
            if (fracJourney >= 0.9F)
            {
                startTime[i] = Time.time;
                states[i] = (states[i] + 1) % nextWaypoint.Length;
                if (states[i] == 0)
                {
                    direction[i] = Mathf.Pow(-1, Random.Range(0, 2));
                }
                startPosition[i] = new Vector3(nextWaypoint[states[i]].x * direction[i] * multiplicator[i], nextWaypoint[states[i]].y * multiplicator[i], nextWaypoint[states[i]].z);
                endPosition[i] = new Vector3(nextWaypoint[(states[i] + 1) % nextWaypoint.Length].x * direction[i] * multiplicator[i], nextWaypoint[(states[i] + 1) % nextWaypoint.Length].y * multiplicator[i], nextWaypoint[(states[i] + 1) % nextWaypoint.Length].z);
                journeyLength[i] = Vector3.Distance(startPosition[i], endPosition[i]);
            }
        }
        particleSystem.SetParticles(leaf, leaf.Length);
    }
}