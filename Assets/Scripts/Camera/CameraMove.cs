﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

    float _time;
    public float magnitude;

    public void MakeShake()
    {
        StopCoroutine("shake");
        StartCoroutine(shake(1.5f));
    }

    public void MakeShake(float time)
    {
        StopCoroutine("shake");
        StartCoroutine(shake(time));
    }

    private IEnumerator shake(float _time)
    {

        float elapsed = 0.0f;

        Vector3 originalCamPos = Camera.main.transform.position;

        while (elapsed < _time)
        {

            elapsed += Time.deltaTime;

            float percentComplete = elapsed / _time;
            float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

            float x = Random.value * 2.0f - 1.0f;
            float y = Random.value * 2.0f - 1.0f;
            x *= magnitude * damper;
            y *= magnitude * damper;

            Camera.main.transform.position = new Vector3(x, y, originalCamPos.z);
            yield return null;
        }

        Camera.main.transform.position = originalCamPos;
    }
}
