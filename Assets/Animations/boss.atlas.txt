
boss.png
format: RGBA8888
filter: Linear,Linear
repeat: none
head
  rotate: true
  xy: 132, 17
  size: 45, 53
  orig: 45, 53
  offset: 0, 0
  index: -1
katana
  rotate: true
  xy: 2, 42
  size: 20, 128
  orig: 20, 128
  offset: 0, 0
  index: -1
left_h
  rotate: false
  xy: 291, 2
  size: 22, 22
  orig: 22, 22
  offset: 0, 0
  index: -1
left_hand
  rotate: true
  xy: 416, 42
  size: 20, 43
  orig: 20, 43
  offset: 0, 0
  index: -1
left_shoulder
  rotate: true
  xy: 61, 12
  size: 28, 48
  orig: 28, 48
  offset: 0, 0
  index: -1
leg
  rotate: false
  xy: 375, 36
  size: 39, 26
  orig: 39, 26
  offset: 0, 0
  index: -1
lower_left_leg
  rotate: true
  xy: 2, 10
  size: 30, 57
  orig: 30, 57
  offset: 0, 0
  index: -1
neck
  rotate: true
  xy: 111, 19
  size: 21, 16
  orig: 21, 16
  offset: 0, 0
  index: -1
pelvis
  rotate: false
  xy: 240, 18
  size: 49, 44
  orig: 49, 44
  offset: 0, 0
  index: -1
stomac
  rotate: false
  xy: 339, 26
  size: 34, 36
  orig: 34, 36
  offset: 0, 0
  index: -1
torse
  rotate: false
  xy: 187, 12
  size: 51, 50
  orig: 51, 50
  offset: 0, 0
  index: -1
upper_left_leg
  rotate: true
  xy: 291, 26
  size: 36, 46
  orig: 36, 46
  offset: 0, 0
  index: -1
